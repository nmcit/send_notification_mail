# -*- coding: utf-8 -*-


{
    'name': 'Mail notification setting',
    'version': '1.0',
    'category': 'Human Resources',
    'author': 'Nova Minds',
    'website': 'www.nmcit.com',
    'summary': 'Mail notification setting',
    'description': """
The purpose of this module is to send Mail notifications to managers to be notified if there is any process happened in the part that they already responsible for to reduce the gap between managers and the daily operations in any system .
    """,
    'depends': ['stock','base','purchase','sale','hr_payroll','project','hr_holidays_multi_levels_approval'],
    'sequence': 16,
    'demo': [],
    'data': [
        'security/mail_security.xml',

        'views/send_mail_view.xml',
        'views/mail_config.xml',
        'security/ir.model.access.csv',

    ],
    'test': [
    ],
    'installable': True,
    'auto_install': False,
    'price': 25,
    'license': 'LGPL-3',
    'currency': 'EUR',
    'images': ['static/description/icon.png'],
    'application': True,
    'qweb': [],
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
