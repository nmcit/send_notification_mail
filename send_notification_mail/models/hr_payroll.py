# -*- coding: utf-8 -*-


import json
import time
from odoo import api, fields, models,_
import odoo.addons.decimal_precision as dp
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from odoo import SUPERUSER_ID, api
import odoo.addons.decimal_precision as dp
import logging
import os

from dateutil.relativedelta import relativedelta
import datetime

_logger = logging.getLogger(__name__)
#----------------------------------------------------------
# Incoterms
#----------------------------------------------------------
class HrPayslip(models.Model):
    _inherit = 'hr.payslip'

    @api.multi
    def action_payslip_done(self):
        super(HrPayslip, self).action_payslip_done()
        body = 'New payslips Confirmed  : ' + self.name

        action_ref = self.env.ref('hr_payroll.%s' % ('action_view_hr_payslip_form',), False)
        portal_link = "%s/web?db=%s#id=%s&action=%s&view_type=form" % (
            self.env['ir.config_parameter'].get_param('web.base.url'),
            self.env.cr.dbname,
            self.id,
            action_ref and action_ref.id or False)
        body += ' <br><br>'
        body += _(
            "<a href=%s>%s</a>") % (
                    portal_link, 'Click here to view')
        self.env['mail.setting'].send_mail_body(self.env.user.company_id.id, 'Notification For payslips Confirmed', body,
                                                'payroll')
