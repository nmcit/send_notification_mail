# -*- coding: utf-8 -*-


from odoo import api, fields, models
import odoo.addons.decimal_precision as dp
import re
from odoo import SUPERUSER_ID, api


class mailsetting(models.Model):
    _name = 'mail.setting'
    _description = 'mail setting'


    @api.model
    def send_mail_body(self,company_id,subject,body,model,em_mail=False):
        message_obj = self.env['mail.message']
        mail_obj = self.env['mail.mail']
        mail_ids = []
        mail_body = ''
        email_crash_flag = 1
        if email_crash_flag == 1:
            mail_subject = subject
            mail_body += '<br>Dear, <br><br> Kindly find the following notification <br><br>'
            emailto = ''
            before=0
            day_month='day'
            mail_ids =  self.env['mail.setting'].search(['&', ('company_id', '=', company_id),'|',
                                                                      ('notification_model', '=', model),('notification_model', '=', 'admin')]
                                                            )
            for mail in mail_ids:
                sss = mail.email
                emailto = emailto + mail.email + ','
                before=mail.before
                day_month=mail.day_month
                if mail.body:
                    mail_body+=mail.body +'<br><br>'

            if em_mail:
                emailto+=em_mail
            mail_body+=body
            mail_body += '<br>'
            DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S.%f"

            if 1:
                mail_body += '<br><br>Thank you, <br>'+self.env['res.company'].browse(company_id).partner_id.name or ''+'. Corporation Team<br><a href="'+self.env['res.company'].browse(company_id).partner_id.website or ''+'">'+self.env['res.company'].browse(company_id).partner_id.website or ''+'</a><br><br> <font color="grey">This is an automated e-mail message, please do not reply directly. If you have any questions contact us.</font>'



                mail_to = emailto  # 'said.yahia@it-syscorp.com;sy111@fayoum.edu.eg;'
                reply_to = self.env['res.company'].browse(company_id).partner_id.email
                mail_from =self.env['res.company'].browse(company_id).partner_id.email

                # if not mail_to:
                #    continue
                # ----- Create relative message (required by mail)
                message_id = message_obj.create( {
                    'type': 'email',
                    'subject': mail_subject,
                })
                # ----- Create mail
                mail_id = mail_obj.create( {
                    'mail_message_id': message_id.id,
                    'state': 'outgoing',
                    'email_from': mail_from,
                    'email_to': mail_to,
                    'reply_to': reply_to,
                    'body_html': mail_body,
                })
                mail_id.send()
                mail_ids = [mail_id.id, ]
                if not mail_ids:
                    return False
                email_sent = mail_obj.send( mail_ids)
                if email_sent:
                    email_crash_flag = 0
        return True

    # def ValidateEmail(self, cr, uid, ids, email):
    # 	if not email:
    # 		return
    # 	if re.match("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$",email) == None:
    # 		raise osv.except_osv('Invalid Email', 'Please enter a valid email address')
    # 	else:
    # 		return True


    @api.multi
    def _validate_email(self):
        if re.match("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$", self.email) == None:
            return False
        else:
            return True

    name = fields.Char('Description', required=True)


    email = fields.Char('Mail', required=True)
    active = fields.Boolean('Active')
    notification_model = fields.Selection([
        ('product', 'Products'),
        ('po', 'Purchase order'),
        ('so', 'Sales order'),
        ('invoice', 'invoice'),
        ('admin', 'Admin'),
        ('payroll', 'Payroll'),
        ('project', 'Project'),
        ('leave', 'Leave'),

    ], 'Notification Model', select=True)
    before = fields.Integer('Before')
    day_month = fields.Selection([
        ('day', 'Days'),
        ('month', 'Months'),

    ], 'Day/Month')
    body=fields.Text('Body')
    company_id = fields.Many2one('res.company', string='Company',  default=lambda self: self.env.user.company_id)

    _constraints = [
        (_validate_email, 'Please enter a valid email address.', ['email']),
    ]
