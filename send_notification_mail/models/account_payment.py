# -*- coding: utf-8 -*-


import json
import time
from odoo import api, fields, models
import odoo.addons.decimal_precision as dp
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from odoo import SUPERUSER_ID, api
import odoo.addons.decimal_precision as dp
import logging
import os

from dateutil.relativedelta import relativedelta
import datetime

_logger = logging.getLogger(__name__)
#----------------------------------------------------------
# Incoterms
#----------------------------------------------------------
class account_payment(models.Model):
    _inherit = 'account.payment'

    @api.multi
    def post(self):
        super(account_payment, self).post()
        for rec in self:
            type=''

            if 0:#rec.payment_type == 'transfer':
                type=' Transfer '
            else:
                if rec.partner_type == 'customer':
                    body='Receive Money From Customer '+rec.partner_id.name+' amount: '+str(rec.amount)
                    # if rec.payment_type == 'inbound':
                    #     sequence_code = 'account.payment.customer.invoice'
                    # if rec.payment_type == 'outbound':
                    #     sequence_code = 'account.payment.customer.refund'
                if rec.partner_type == 'supplier':
                    body='Send Money To Vendor '+rec.partner_id.name+' amount: '+str(rec.amount)

                    # if rec.payment_type == 'inbound':
                    #     sequence_code = 'account.payment.supplier.refund'
                    # if rec.payment_type == 'outbound':
                    #     sequence_code = 'account.payment.supplier.invoice'

            self.env['mail.setting'].send_mail_body(self.env.user.company_id.id, 'Notification For Send/Receive Money', body,'invoice')
