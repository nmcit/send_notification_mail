# -*- coding: utf-8 -*-


import json
import time
from odoo import api, fields, models,_
import odoo.addons.decimal_precision as dp
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from odoo import SUPERUSER_ID, api
import odoo.addons.decimal_precision as dp
import logging
import os

from dateutil.relativedelta import relativedelta
import datetime

_logger = logging.getLogger(__name__)
#----------------------------------------------------------
# Incoterms
#----------------------------------------------------------
class Task(models.Model):
    _inherit = "project.task"
    @api.model
    def create(self, values):
        res = super(Task, self).create(values)
        body='now Task '+res.name+'   Created For user  : '+res.user_id.name
        action_ref = self.env.ref('project.%s' % ('action_view_task',), False)
        portal_link = "%s/web?db=%s#id=%s&action=%s&view_type=form" % (
            self.env['ir.config_parameter'].get_param('web.base.url'),
            self.env.cr.dbname,
            res.id,
            action_ref and action_ref.id or False)
        body += ' <br><br>'
        body += _(
            "<a href=%s>%s</a>") % (
                    portal_link, 'Click here to view')
        self.env['mail.setting'].send_mail_body(self.env.user.company_id.id,'Notification For Create Task',body,'project',res.user_id.email)
        return res
    @api.multi
    def write(self, vals):
        res = super(Task, self).write(vals)
        # stage change: update date_last_stage_update
        if 'stage_id' in vals:
            body = 'Task ' + self.name + ' its stage Change to : ' + self.env['project.task.type'].browse(vals.get('stage_id'))[0].name
            action_ref = self.env.ref('project.%s' % ('action_view_task',), False)
            portal_link = "%s/web?db=%s#id=%s&action=%s&view_type=form" % (
                self.env['ir.config_parameter'].get_param('web.base.url'),
                self.env.cr.dbname,
                self.id,
                action_ref and action_ref.id or False)
            body += ' <br><br>'
            body += _(
                "<a href=%s>%s</a>") % (
                        portal_link, 'Click here to view')
            self.env['mail.setting'].send_mail_body(self.env.user.company_id.id, 'Notification For Change Task stage',
                                                    body,
                                                    'project', self.user_id.email)
        return res

    @api.model
    def check_task_deadline(self):
        DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S.%f"
        current_time = datetime.datetime.now()
        new_current_date = datetime.datetime.strptime(str(current_time), DATETIME_FORMAT).strftime('%Y-%m-%d')
        tasks=self.search([('date_deadline','=',str(new_current_date))])
        body=''
        for each in tasks:
            body += 'Task ' + each.name + ' For user  : ' + each.user_id.name +' its deadline: '+each.date_deadline+'  <br><br>'
            action_ref = self.env.ref('project.%s' % ('action_view_task',), False)
            portal_link = "%s/web?db=%s#id=%s&action=%s&view_type=form" % (
                self.env['ir.config_parameter'].get_param('web.base.url'),
                self.env.cr.dbname,
                each.id,
                action_ref and action_ref.id or False)
            body += ' <br><br>'
            body += _(
                "<a href=%s>%s</a>") % (
                        portal_link, 'Click here to view')
            self.env['mail.setting'].send_mail_body(self.env.user.company_id.id, 'Notification For Tasks deadline', body,
                                                'project')
