# -*- coding: utf-8 -*-


import json
import time
from odoo import api, fields, models,_
import odoo.addons.decimal_precision as dp
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from odoo import SUPERUSER_ID, api
import odoo.addons.decimal_precision as dp
import logging
import os

from dateutil.relativedelta import relativedelta
import datetime

_logger = logging.getLogger(__name__)
#----------------------------------------------------------
# Incoterms
#----------------------------------------------------------
class purchase_order(models.Model):
    _inherit = 'purchase.order'

    @api.model
    def create(self, values):
        res = super(purchase_order, self).create(values)
        body='now PO Created and its number : '+res.name
        action_ref = self.env.ref('purchase.%s' % ('purchase_rfq',), False)
        portal_link = "%s/web?db=%s#id=%s&action=%s&view_type=form" % (
            self.env['ir.config_parameter'].get_param('web.base.url'),
            self.env.cr.dbname,
            res.id,
            action_ref and action_ref.id or False)
        body += ' <br><br>'
        body += _(
            "<a href=%s>%s</a>") % (
                    portal_link, 'Click here to view')
        self.env['mail.setting'].send_mail_body(self.env.user.company_id.id,'Notification For Create PO',body,'po')
        return res

    @api.multi
    def button_confirm(self):
        super(purchase_order, self).button_confirm()

        for order in self:
            body = 'PO confirmed and its number : ' + order.name
            action_ref = self.env.ref('purchase.%s' % ('purchase_rfq',), False)
            portal_link = "%s/web?db=%s#id=%s&action=%s&view_type=form" % (
                self.env['ir.config_parameter'].get_param('web.base.url'),
                self.env.cr.dbname,
                order.id,
                action_ref and action_ref.id or False)
            body += ' <br><br>'
            body += _(
                "<a href=%s>%s</a>") % (
                        portal_link, 'Click here to view')
            self.env['mail.setting'].send_mail_body(self.env.user.company_id.id, 'Notification For Confirmed PO', body,
                                                    'po')
    @api.multi
    def button_cancel(self):
        super(purchase_order, self).button_cancel()

        for order in self:
            body = 'PO Cancelled and its number : ' + order.name
            action_ref = self.env.ref('purchase.%s' % ('purchase_rfq',), False)
            portal_link = "%s/web?db=%s#id=%s&action=%s&view_type=form" % (
                self.env['ir.config_parameter'].get_param('web.base.url'),
                self.env.cr.dbname,
                order.id,
                action_ref and action_ref.id or False)
            body += ' <br><br>'
            body += _(
                "<a href=%s>%s</a>") % (
                        portal_link, 'Click here to view')
            self.env['mail.setting'].send_mail_body(self.env.user.company_id.id, 'Notification For Cancelled PO', body,
                                                    'po')
