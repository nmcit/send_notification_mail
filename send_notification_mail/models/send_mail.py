# -*- coding: utf-8 -*-


import json
import time
from odoo import api, fields, models
import odoo.addons.decimal_precision as dp
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from odoo import SUPERUSER_ID, api
import odoo.addons.decimal_precision as dp
import logging
import os

from dateutil.relativedelta import relativedelta
import datetime

_logger = logging.getLogger(__name__)
#----------------------------------------------------------
# Incoterms
#----------------------------------------------------------
class report_automation(models.Model):
    _inherit = 'stock.production.lot'
    @api.model
    def check_expiry_and_send_mail(self):
        message_obj = self.env['mail.message']
        mail_obj = self.env['mail.mail']
        mail_ids = []
        mail_body = ''
        email_crash_flag = 1
        if email_crash_flag == 1:

            emailto = ''
            before=0
            day_month='day'
            mail_ids = self.env['mail.setting'].search(['&', ('company_id', '=', self.env.user.company_id.id), '|',
                                                        ('notification_model', '=', 'product'),
                                                        ('notification_model', '=', 'admin')])

            for mail in mail_ids:
                sss = mail.email
                emailto = emailto + mail.email + ','
                before=mail.before
                day_month=mail.day_month
                if mail.body:
                    mail_body += mail.body + '<br><br>'


            mail_subject = 'product expiry date reminder'
            mail_body += '<br>Dear, <br><br> Kindly find the products which expired date <br><br>'
            mail_body += '<br>'
            DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S.%f"
            current_time = datetime.datetime.now()
            new_current_time = current_time
            if day_month=='day':
                new_current_time = current_time + relativedelta(days=int(before))
            if day_month=='month':
                new_current_time = current_time + relativedelta(months=int(before))
            #new_current_time = current_time + relativedelta(months=3)
            new_current_date = datetime.datetime.strptime(str(new_current_time), DATETIME_FORMAT).strftime('%Y-%m-%d')
            sqlstr = "select product_id,removal_date,name from stock_production_lot  where  date(removal_date)='" + str(
                new_current_date) + "'"
            self.env.cr.execute(sqlstr)

            lot_lst = self.env.cr.fetchall()
            bexist_product=False
            for lot in lot_lst:  # self.pool.get("stock.production.lot").browse(cr, uid, lots_ids):
                bexist_product=True
                mail_body += '<br>'
                mail_body += '** Product :: '
                if self.env["product.product"].browse(lot[0]).name:
                    mail_body += self.env["product.product"].browse(lot[0]).name
                mail_body += ' --- With Serial Number :: '
                if lot[2]:
                    mail_body += lot[2]
                mail_body += ' Will Expired in :: '

                if lot[1]:
                    mail_body += lot[1]
                mail_body += '<br>'
                # sqlquant = "select sum (qty) from stock_quant  JOIN stock_location dest_location ON stock_quant.location_id = dest_location.id where product_id=" + \
                #            str(lot[0]) + " and dest_location.usage in ('internal', 'transit')"
                # self.env.cr.execute(sqlquant)
                #
                # quan_lst = self.env.cr.fetchall()
                mail_body += ' and its quantity on hand is :: '

                if self.env["product.product"].browse(lot[0]):
                    mail_body += str(self.env["product.product"].browse(lot[0]).qty_available)
                mail_body += '<br>'
            if bexist_product:
                mail_body += '<br><br>Thank you, <br>'+self.env['res.partner'].browse(SUPERUSER_ID).company_id.name+'. Corporation Team<br><a href="'+self.env['res.partner'].browse(SUPERUSER_ID).company_id.website+'">'+self.env['res.partner'].browse(SUPERUSER_ID).company_id.website+'</a><br><br> <font color="grey">This is an automated e-mail message, please do not reply directly. If you have any questions contact us.</font>'



                mail_to = emailto  # 'said.yahia@it-syscorp.com;sy111@fayoum.edu.eg;'
                reply_to = self.env['res.partner'].browse(SUPERUSER_ID).email
                mail_from =self.env['res.partner'].browse(SUPERUSER_ID).email

                # if not mail_to:
                #    continue
                # ----- Create relative message (required by mail)
                message_id = message_obj.create( {
                    'type': 'email',
                    'subject': mail_subject,
                })
                # ----- Create mail
                mail_id = mail_obj.create( {
                    'mail_message_id': message_id.id,
                    'state': 'outgoing',
                    'email_from': mail_from,
                    'email_to': mail_to,
                    'reply_to': reply_to,
                    'body_html': mail_body,
                })
                mail_id.send()
                mail_ids = [mail_id.id, ]
                if not mail_ids:
                    return False
                email_sent = mail_obj.send( mail_ids)
                if email_sent:
                    email_crash_flag = 0
        return True
