# -*- coding: utf-8 -*-


import json
import time
from odoo import api, fields, models,_
import odoo.addons.decimal_precision as dp
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from odoo import SUPERUSER_ID, api
import odoo.addons.decimal_precision as dp
import logging
import os

from dateutil.relativedelta import relativedelta
import datetime
from urlparse import urljoin
from odoo.addons.website.models.website import slug

_logger = logging.getLogger(__name__)
#----------------------------------------------------------
# Incoterms
#----------------------------------------------------------
class sale_order(models.Model):
    _inherit = 'sale.order'

    @api.model
    def create(self, values):
        res = super(sale_order, self).create(values)
        action_ref = self.env.ref('sale.%s' % ('action_orders',), False)
        portal_link = "%s/web?db=%s#id=%s&action=%s&view_type=form" % (
            self.env['ir.config_parameter'].get_param('web.base.url'),
            self.env.cr.dbname,
            res.id,
            action_ref and action_ref.id or False)

        body='now SO Created and its number : '+res.name+' <br><br>'
        body += _(
            "<a href=%s>%s</a>") % (
                       portal_link, 'Click here to view')
        self.env['mail.setting'].send_mail_body(self.env.user.company_id.id,'Notification For Create SO',body,'so')
        return res

    @api.multi
    def action_confirm(self):
        super(sale_order, self).action_confirm()

        for order in self:
            action_ref = self.env.ref('sale.%s' % ('action_orders',), False)
            portal_link = "%s/web?db=%s#id=%s&action=%s&view_type=form" % (
                self.env['ir.config_parameter'].get_param('web.base.url'),
                self.env.cr.dbname,
                order.id,
                action_ref and action_ref.id or False)
            body = 'SO confirmed and its number : ' + order.name
            body +=' <br><br>'
            body += _(
                "<a href=%s>%s</a>") % (
                        portal_link, 'Click here to view')
            self.env['mail.setting'].send_mail_body(self.env.user.company_id.id, 'Notification For Confirmed SO', body,
                                                    'so')
    @api.multi
    def action_cancel(self):
        super(sale_order, self).action_cancel()

        for order in self:
            action_ref = self.env.ref('sale.%s' % ('action_orders',), False)
            portal_link = "%s/web?db=%s#id=%s&action=%s&view_type=form" % (
                self.env['ir.config_parameter'].get_param('web.base.url'),
                self.env.cr.dbname,
                order.id,
                action_ref and action_ref.id or False)
            body = 'SO Cancelled and its number : ' + order.name
            body += ' <br><br>'
            body += _(
                "<a href=%s>%s</a>") % (
                        portal_link, 'Click here to view')
            self.env['mail.setting'].send_mail_body(self.env.user.company_id.id, 'Notification For Cancelled SO', body,
                                                    'so')
