# -*- coding: utf-8 -*-


import json
import time
from odoo import api, fields, models,_
import odoo.addons.decimal_precision as dp
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from odoo import SUPERUSER_ID, api
import odoo.addons.decimal_precision as dp
import logging
import os

from dateutil.relativedelta import relativedelta
import datetime

_logger = logging.getLogger(__name__)
#----------------------------------------------------------
# Incoterms
#----------------------------------------------------------
class account_invoice(models.Model):
    _inherit = 'account.invoice'

    @api.model
    def create(self, values):
        res = super(account_invoice, self).create(values)
        if res.type == 'out_invoice':
            body='now Invoice Created For Customer  : '+res.partner_id.name

        else:
            body='now Invoice Created For Vendor  : '+res.partner_id.name

        action_ref = self.env.ref('account.%s' % ('action_invoice_tree1',), False)
        portal_link = "%s/web?db=%s#id=%s&action=%s&view_type=form" % (
            self.env['ir.config_parameter'].get_param('web.base.url'),
            self.env.cr.dbname,
            res.id,
            action_ref and action_ref.id or False)
        body += ' <br><br>'
        body += _(
            "<a href=%s>%s</a>") % (
                    portal_link, 'Click here to view')
        self.env['mail.setting'].send_mail_body(self.env.user.company_id.id,'Notification For Create invoice',body,'invoice')
        return res

    @api.multi
    def action_invoice_open(self):
        to_open_invoices=super(account_invoice, self).action_invoice_open()

        for order in self:
            if order.type == 'out_invoice':
                body = 'Validate Invoice number '+order.number+' For Customer  : ' + order.partner_id.name +" : <a href=# data-oe-model=account.invoice data-oe-id="+str(order.id)+">"+str(order.number)+"</a>"
            else:
                body = 'Validate Invoice number '+order.number+' For Vendor  : ' + order.partner_id.name+" : <a href=# data-oe-model=account.invoice data-oe-id="+str(order.id)+">"+str(order.number)+"</a>"

            action_ref = self.env.ref('account.%s' % ('action_invoice_tree1',), False)
            portal_link = "%s/web?db=%s#id=%s&action=%s&view_type=form" % (
                self.env['ir.config_parameter'].get_param('web.base.url'),
                self.env.cr.dbname,
                order.id,
                action_ref and action_ref.id or False)
            body += ' <br><br>'
            body += _(
                "<a href=%s>%s</a>") % (
                        portal_link, 'Click here to view')
            self.env['mail.setting'].send_mail_body(self.env.user.company_id.id, 'Notification For Validate Invoice', body,'invoice')
        return to_open_invoices

    @api.multi
    def action_invoice_cancel(self):
        res=super(account_invoice, self).action_invoice_cancel()

        for order in self:
            if order.type == 'out_invoice':
                if order.number:
                    body = 'Cancel Invoice number ' + order.number + ' For Vendor  : ' + order.partner_id.name
                else:
                    body = 'Cancel Invoice  For Customer  : ' + order.partner_id.name

            action_ref = self.env.ref('account.%s' % ('action_invoice_tree1',), False)
            portal_link = "%s/web?db=%s#id=%s&action=%s&view_type=form" % (
                self.env['ir.config_parameter'].get_param('web.base.url'),
                self.env.cr.dbname,
                order.id,
                action_ref and action_ref.id or False)
            body += ' <br><br>'
            body += _(
                "<a href=%s>%s</a>") % (
                        portal_link, 'Click here to view')
            self.env['mail.setting'].send_mail_body(self.env.user.company_id.id, 'Notification For Cancel Inoice',
                                                        body, 'invoice')
        return res