# -*- coding: utf-8 -*-


import json
import time
from odoo import api, fields, models,_
import odoo.addons.decimal_precision as dp
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from odoo import SUPERUSER_ID, api
import odoo.addons.decimal_precision as dp
import logging
import os

from dateutil.relativedelta import relativedelta
import datetime

_logger = logging.getLogger(__name__)
#----------------------------------------------------------
# Incoterms
#----------------------------------------------------------
class Holidays(models.Model):
    _inherit = "hr.holidays"

    @api.model
    def create(self, values):
        res = super(Holidays, self).create(values)
        if res.pending_approver:
            body='This leave '+res.display_name+' and need your approve'

            action_ref = self.env.ref('hr_holidays.%s' % ('open_employee_leaves',), False)
            portal_link = "%s/web?db=%s#id=%s&action=%s&view_type=form" % (
                self.env['ir.config_parameter'].get_param('web.base.url'),
                self.env.cr.dbname,
                res.id,
                action_ref and action_ref.id or False)
            body += ' <br><br>'
            body += _(
                "<a href=%s>%s</a>") % (
                        portal_link, 'Click here to view')

            self.env['mail.setting'].send_mail_body(self.env.user.company_id.id,'Notification For Leave Approve',body,'leave',res.pending_approver.user_id.email or False)
        return res

    @api.multi
    def action_approve(self):
        res = super(Holidays, self).action_approve()

        for holiday in self:
            if holiday.pending_approver:
                body = 'This leave ' + holiday.display_name + ' and need your approve'
                user = holiday.pending_approver.user_id

                action_ref = self.env.ref('hr_holidays.%s' % ('open_employee_leaves',), False)
                portal_link = "%s/web?db=%s#id=%s&action=%s&view_type=form" % (
                    self.env['ir.config_parameter'].get_param('web.base.url'),
                    self.env.cr.dbname,
                    holiday.id,
                    action_ref and action_ref.id or False)
                body += ' <br><br>'
                body += _(
                    "<a href=%s>%s</a>") % (
                            portal_link, 'Click here to view')

                print (user, holiday.pending_approver, user.email)
                self.env['mail.setting'].send_mail_body(self.env.user.company_id.id, 'Notification For Leave Approve', body,
                                                        'leave', user.email)
            else:
                body = 'This leave ' + holiday.display_name + ' and final approved'
                user = holiday.employee_id

                action_ref = self.env.ref('hr_holidays.%s' % ('open_employee_leaves',), False)
                portal_link = "%s/web?db=%s#id=%s&action=%s&view_type=form" % (
                    self.env['ir.config_parameter'].get_param('web.base.url'),
                    self.env.cr.dbname,
                    holiday.id,
                    action_ref and action_ref.id or False)
                body += ' <br><br>'
                body += _(
                    "<a href=%s>%s</a>") % (
                            portal_link, 'Click here to view')

                print (user, holiday.pending_approver, user.work_email)
                self.env['mail.setting'].send_mail_body(self.env.user.company_id.id, 'Notification For Leave Final Approve',
                                                        body,
                                                        'leave', user.work_email)

    @api.multi
    def write(self, vals):
        res = super(Holidays, self).write(vals)
        # stage change: update date_last_stage_update
        # if res and 'pending_approver' in vals:
        #     body='This leave '+self.display_name+' and need your approve'
        #     user=self.env['hr.employee'].browse(vals.get('pending_approver'))[0].user_id
        #
        #     action_ref = self.env.ref('hr_holidays.%s' % ('open_employee_leaves',), False)
        #     portal_link = "%s/?db=%s#id=%s&action=%s&view_type=form" % (
        #         self.env['ir.config_parameter'].get_param('web.base.url'),
        #         self.env.cr.dbname,
        #         self.id,
        #         action_ref and action_ref.id or False)
        #     body += ' <br><br>'
        #     body += _(
        #         "<a href=%s>%s</a>") % (
        #                 portal_link, 'Click here to view')
        #
        #     print (user,vals.get('pending_approver'),user.email)
        #     self.env['mail.setting'].send_mail_body(self.env.user.company_id.id, 'Notification For Leave Approve', body,
        #                                             'leave', user.email)

        return res

    @api.multi
    def action_refuse(self):
        super(Holidays, self).action_refuse()
        for each in self:
            body = 'This leave ' + self.display_name + ' Refused ....'
            user = each.employee_id
            action_ref = self.env.ref('hr_holidays.%s' % ('open_employee_leaves',), False)
            portal_link = "%s/web?db=%s#id=%s&action=%s&view_type=form" % (
                self.env['ir.config_parameter'].get_param('web.base.url'),
                self.env.cr.dbname,
                each.id,
                action_ref and action_ref.id or False)
            body += ' <br><br>'
            body += _(
                "<a href=%s>%s</a>") % (
                        portal_link, 'Click here to view')

            self.env['mail.setting'].send_mail_body(self.env.user.company_id.id, 'Notification For Leave Refused', body,
                                                    'leave', user.work_email)